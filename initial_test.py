# -*- coding: utf-8 -*-
from core.Game import GameManager
from tests_classes.Scenes import MenuScene

#A dictionary to make it easy to know when a is is held down
#its values will be updated everytime a event of KEYUP or KEYDOWN is called
KEY_PRESSED = {
    'up': False,
    'right': False,
    'down': False,
    'left': False,
    'space': False
}
KEY_DOWN = {
    'up': False,
    'right': False,
    'down': False,
    'left': False,
    'space': False
}
KEY_UP = {
    'up': False,
    'right': False,
    'down': False,
    'left': False,
    'space': False
}

game_manager = GameManager(MenuScene(), 'Arrow keys move - Space Reset', 800, 600)
game_manager.run()
