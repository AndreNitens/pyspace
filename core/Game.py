# -*- coding: utf-8 -*-
import sys
import pygame

from pygame.locals import *

from core.Scene import SceneManager


class GameManager(object):
    """
    Class responsible for managing the main GameLoop, updates the inputs and call 'update' and 'draw'
    for everything on the game

    Andre - 17/01/2016
    """

    scene_manager = None
    DISPLAYSURF = None
    EVENTS = None
    exit = False

    def __init__(self, initial_scene, window_name='Test', window_width=640, window_height=480):
        pygame.init()
        self.DISPLAYSURF = pygame.display.set_mode((window_width, window_height))
        self.EVENTS = pygame.event.get()
        pygame.display.set_caption(window_name)
        self.scene_manager = SceneManager(initial_scene)

    def update(self):
        self.update_events()
        self.scene_manager.update()

    def draw(self):
        self.DISPLAYSURF.fill((0, 0, 0))
        self.scene_manager.draw(self.DISPLAYSURF)

    def change_scene(self):
        self.scene_manager.change_scene()

    def exit_game(self):
        pygame.quit()
        sys.exit()

    def update_events(self):
        from initial_test import KEY_PRESSED, KEY_DOWN, KEY_UP

        # The flag for a key down or up should last only one frame
        # So, in the beggining of everyframe it is turned off
        for key in KEY_DOWN.keys():
            KEY_DOWN[key] = False
            KEY_UP[key] = False

        self.EVENTS = pygame.event.get()
        for event in self.EVENTS:
            if event.type == pygame.KEYDOWN:
                if event.key == pygame.K_LEFT:
                    KEY_PRESSED['left'] = True
                    KEY_DOWN['left'] = True
                elif event.key == pygame.K_RIGHT:
                    KEY_PRESSED['right'] = True
                    KEY_DOWN['right'] = True
                elif event.key == pygame.K_UP:
                    KEY_PRESSED['up'] = True
                    KEY_DOWN['up'] = True
                elif event.key == pygame.K_DOWN:
                    KEY_PRESSED['down'] = True
                    KEY_DOWN['down'] = True
                elif event.key == pygame.K_SPACE:
                    KEY_PRESSED['space'] = True
                    KEY_DOWN['space'] = True

            if event.type == pygame.KEYUP:
                if event.key == pygame.K_LEFT:
                    KEY_PRESSED['left'] = False
                    KEY_UP['left'] = True
                elif event.key == pygame.K_RIGHT:
                    KEY_PRESSED['right'] = False
                    KEY_UP['right'] = True
                elif event.key == pygame.K_UP:
                    KEY_PRESSED['up'] = False
                    KEY_UP['up'] = True
                elif event.key == pygame.K_DOWN:
                    KEY_PRESSED['down'] = False
                    KEY_UP['down'] = True
                elif event.key == pygame.K_SPACE:
                    KEY_PRESSED['space'] = False
                    KEY_UP['space'] = True

            if event.type == QUIT:
                self.exit = True

    def run(self):
        # Begin and control all the GameLoop
        while not self.exit:
            self.update()
            self.draw()
            self.change_scene()
            pygame.display.update()

        self.exit_game()
