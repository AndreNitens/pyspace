# -*- coding: utf-8 -*-
import pygame

from pygame.locals import *


class SceneManager(object):
    """
    Class to control scene flow and the changes of scenes

    Andre - 17/01/2016
    """

    actual_scene = None

    def __init__(self, initial_scene):
        self.actual_scene = initial_scene
        self.actual_scene.load()

    def update(self):
        self.actual_scene.update()

    def draw(self, target):
        self.actual_scene.draw(target)

    def change_scene(self):
        next_scene = self.actual_scene.next_scene()
        if next_scene != self.actual_scene:
            self.actual_scene.unload()
            self.actual_scene = next_scene
            self.actual_scene.load()


class Scene(object):
    """
    Reference class of how a Scene should be implemented

    Andre - 17/01/2016
    """

    object_manager = []

    def load(self):
        pass

    def update(self):
        for object in self.object_manager:
            object.update()

    def draw(self, target):
        for object in self.object_manager:
            object.draw(target)

    def next_scene(self):
        return self

    def unload(self):
        pass
