# -*- coding: utf-8 -*-
import pygame

from pygame.locals import *


class Sprite(object):
    """
    Class for handling simple Sprite operations

    Andre - 15/01/2016
    """

    image = None
    x = 0
    y = 0

    def __init__(self, image_source=None):
        if image_source is not None:
            self.load_image(image_source)

    def load_image(self, image_name):
        try:
            self.image = pygame.image.load(image_name)
        except pygame.error, message:
            print 'Could not load image:', image_name
            raise SystemExit, message

    def draw(self, x, y, destiny):
        destiny.blit(self.image, (x, y))


class GameObject(object):
    """
    Class to handle simple GameObject operations, like position and sprite

    Andre - 15/01/2016
    """

    x = 0
    y = 0
    sprite = None

    def __init__(self, image_name=None, x=0, y=0):
        self.sprite = Sprite(image_name)
        self.set_position(x, y)

    def move(self, x, y):
        self.x += x
        self.y += y

    def get_position(self):
        return self.x, self.y

    def set_position(self, x, y):
        self.x, self.y = x, y

    def update(self):
        pass

    def draw(self, destiny=None):
        if self.sprite is not None:
            self.sprite.draw(self.x, self.y, destiny)
