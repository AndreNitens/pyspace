# -*- coding: utf-8 -*-
from core.Scene import Scene
from Objects import Porconauta


class MenuScene(Scene):
    """
    Very basic Scene with just a porconauta in it

    Andre - 17/01/2016
    """

    def load(self):
        self.object_manager.append(Porconauta())

    def next_scene(self):
        from initial_test import KEY_DOWN

        if KEY_DOWN['space']:
            return MenuScene()
        else:
            return self

    def unload(self):
        del self.object_manager[:]
