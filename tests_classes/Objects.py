# -*- coding: utf-8 -*-
from core.GameObjects import GameObject


class Porconauta(GameObject):
    """
    A pig that move with the arrow keys

    Andre - 17/01/2016
    """

    def __init__(self):
        super(Porconauta, self).__init__('assets/images/initial_test/porconauta.png')

    def update(self):
        from initial_test import KEY_PRESSED
        if KEY_PRESSED['up']:
            self.move(0, -2)
        if KEY_PRESSED['right']:
            self.move(2, 0)
        if KEY_PRESSED['down']:
            self.move(0, 2)
        if KEY_PRESSED['left']:
            self.move(-2, 0)
